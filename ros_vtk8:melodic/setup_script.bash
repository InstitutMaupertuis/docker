#!/bin/bash
apt update
apt install -y wget unzip

# Robot Operating System
apt full-upgrade -y
apt install -y python-catkin-tools

# Visualization ToolKit
apt install -y libglew-dev libxt-dev
wget http://www.vtk.org/files/release/8.2/VTK-8.2.0.zip -q
unzip VTK-8.2.0.zip >/dev/null
rm VTK-8.2.0.zip
mkdir /vtk_build
cd /vtk_build
cmake -DCMAKE_BUILD_TYPE=Release ../VTK-8.2.0
make -j4 install
ldconfig
cd /
rm -rf /VTK-8.2.0 /vtk_build
