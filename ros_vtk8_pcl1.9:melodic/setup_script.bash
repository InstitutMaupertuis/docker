#!/bin/bash
apt update

# Robot Operating System
apt full-upgrade -y

# Point Cloud Library
apt install -y libboost-all-dev libflann-dev libeigen3-dev libqhull-dev
wget https://github.com/PointCloudLibrary/pcl/archive/pcl-1.9.1.zip -q
unzip pcl-1.9.1.zip >/dev/null
rm pcl-1.9.1.zip
mkdir /pcl_build
cd /pcl_build
cmake -DCMAKE_BUILD_TYPE=MinSizeRel -DPCL_ONLY_CORE_POINT_TYPES=ON -DWITH_OPENGL=FALSE ../pcl-pcl-1.9.1
make -j4 install
ldconfig
cd /
rm -rf /pcl-pcl-1.9.1 /pcl_build
