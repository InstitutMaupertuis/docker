[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# Description
This repository contains the `Dockerfile` used to generate the following repositories:
- https://hub.docker.com/r/victorlamoine/ros_sphinx_latexpdf/
- https://hub.docker.com/r/victorlamoine/ros_vtk8/
- https://hub.docker.com/r/victorlamoine/ros_vtk8_sphinx_latexpdf/
- https://hub.docker.com/r/victorlamoine/ros_vtk8_pcl1.9/
- https://hub.docker.com/r/victorlamoine/opencv_cmake_git/

Before creating and pushing new image, update local image:
```
docker pull ros:melodic-robot
docker pull ubuntu:bionic
```

## Create/push an image
```bash
cd ros_sphinx_latexpdf:melodic
docker build -t victorlamoine/ros_sphinx_latexpdf:melodic .
docker push victorlamoine/ros_sphinx_latexpdf
```

```bash
cd ros_vtk8:melodic
docker build -t victorlamoine/ros_vtk8:melodic .
docker push victorlamoine/ros_vtk8
```

```bash
cd ros_vtk8_sphinx_latexpdf:melodic
docker build -t victorlamoine/ros_vtk8_sphinx_latexpdf:melodic .
docker push victorlamoine/ros_vtk8_sphinx_latexpdf
```

```bash
cd ros_vtk8_pcl1.9:melodic
docker build -t victorlamoine/ros_vtk8_pcl1.9:melodic .
docker push victorlamoine/ros_vtk8_pcl1.9
```

```bash
cd opencv_cmake_git:bionic
docker build -t victorlamoine/opencv_cmake_git:bionic .
docker push victorlamoine/opencv_cmake_git
```
