#!/bin/bash
apt update

# Robot Operating System
apt full-upgrade -y

# Sphinx and LatexPDF
apt install -y python-pip latexmk texlive-base texlive-latex-recommended texlive-fonts-recommended texlive-latex-extra
pip install --no-cache-dir sphinx==1.7.4 sphinx_rtd_theme
sphinx-build --version
